import json
import os
import pandas as pd
import datetime
import sys
import numpy as np

spitpath = sys.argv[1]
dapsheet = sys.argv[2]
outfile = sys.argv[3]

def convert(o):
    if isinstance(o, np.int64): return int(o)  
    raise TypeError


def get_tfplates(filepath):
    # get tf/i7 plate info
    tfdf = pd.read_excel(filepath, sheet_name="TF+i7 info")
    tfdf.columns = ['tfplate', 'i7plate']
    return tfdf

def get_spitis(filepath):
    spsamples = pd.read_excel(filepath, sheet_name="DATA SHEET")
    # tcols = spsamples.columns[[5,6,7,2,1,0,3]]
    tcols = ['Genus*', 'Species*', 'Strain* (required for microbial products)', 'Sample ID (No edit)', 'Seq Project Name (No edit)', 'Seq Project ID (No edit)', 'Sample Name*']
    # Get rid of first 2 rows
    subsamples = spsamples.loc[2:, tcols]
    # subsamples.columns = ['genus', 'species', 'strain', 'sample-id', 'sequencing-project-name', 'sequencing-project-id', 'sample-name']
    # Create a unique identified for each sample to match with other sheet
    subsamples.rename(columns={"Genus*": "genus", "Species*": "species", "Strain* (required for microbial products)": "strain",
                              "Sample ID (No edit)": "sample-id", "Seq Project Name (No edit)": "sequencing-project-name", 
                              "Seq Project ID (No edit)": "sequencing-project-id", "Sample Name*":"sample-name"}, inplace=True)
    subsamples['mattid'] = subsamples['genus'].astype(str).str.upper() + "_" + subsamples['species'].astype(str).str.upper() + "_" + subsamples['strain'].astype(str).str.upper() 
    return subsamples.reset_index(drop=True)

def get_subxcel(filepath):
    # get sample info
    xsamples = pd.read_excel(filepath, sheet_name="organisms")
    xsamples['mattid'] = xsamples['Genus'].astype(str).str.upper() + "_" + xsamples['Species'].astype(str).str.upper() + "_" + xsamples['Strain'].astype(str).str.upper()
    return xsamples

def get_sample_info(spitpath, testpath):
    subsamples = get_spitis(spitpath)
    # Load excel sample sheet data
    sampledf = get_subxcel(testpath)
    # check for same number of samples in both SPITIS and sample submission sheet
    if len(set(sampledf['mattid'])) != len(set(subsamples['mattid'])):
        print("different number of samples between SPITIS and DapSheet")
        sys.exit("different number of samples in DapSheet SPITIS form")
    # check for unique ids 
    if len(set(sampledf['mattid'])) != len(sampledf['mattid']) or len(set(subsamples['mattid'])) != len(subsamples['mattid']):
        print("non unique sample ids (Genus-Species-Strain) are present")
        sys.exit("non unique sample ids are present")
    # make sure sample ids match up
    if len(set(sampledf['mattid'])) != len(sampledf['mattid']) or len(set(subsamples['mattid'])) != len(subsamples['mattid']):
        print("samples are different between SPITIS and DapSheet")
        sys.exit("non unique sample ids are present")
    sampleinfo = pd.merge(sampledf, subsamples)
    sampleinfo.rename(columns={"Reference ID":"reference", "Annotation ID":"annotation", "Amplification Cycles":"amplification-cycles", "Lot Number":"lot-number", "i5 Index":"i5"}, inplace=True)
    sampleinfo["reference"] = sampleinfo["reference"].astype(str)
    sampleinfo["amplification-cycles"] = sampleinfo["amplification-cycles"].astype(str)
    sampleinfo['lot-number'] = str(datetime.date.today())
    sampleinfo = sampleinfo[["sequencing-project-id", "sequencing-project-name", "sample-id", "sample-name", "genus", "species", "strain", "reference", "annotation", "lot-number", "amplification-cycles", "i5"]]
    sampleinfo['sample-id'] = sampleinfo['sample-id'].astype(int)
    sampleinfo['sequencing-project-id'] = sampleinfo['sequencing-project-id'].astype(int)
    sampleinfo['amplification-cycles'] = sampleinfo['amplification-cycles'].astype(int)
    sampleinfo['sample-name'] = sampleinfo['sample-name'].astype(str)

    return sampleinfo

def get_experiment_info(filepath):
    pooldf = pd.read_excel(dapsheet, sheet_name='library submission', index_col=0, header=None).transpose().reset_index(drop=True)
    pooldf.rename(columns={"molarity (pM)": "molarity-pm", "concentration-pm": "concentration-ng-ul", "Pool avg template size (bp)": "avg-template-size-bp", "Pool volume (uL)": "volume-ul", "Experiment date": "dap_date", 
                           "Final PCR cycles": "number-pcr-cycles-dap-amplification", "TF source genus": "tf_genus",  "TF source species": "tf_species", "TF source strain": "tf_strain", "TF source type": "tf_source",
                           "TF source reference": "tf_reference", "TF source lot number": "tf_lot-number", "Clarity ID": "submitted-by-cid"}, inplace = True)
    #pooldf.columns = ['molarity-pm', 'concentration-ng-ul', 'avg-template-size-bp', 'volume-ul', 'dap_date', 'number-pcr-cycles-dap-amplification', 'tf_genus', 'tf_species', 'tf_strain', 'tf_source', 'tf_reference', 'tf_lot-number']
    pooldf['number-pcr-cycles-dap-amplification'] = pooldf['number-pcr-cycles-dap-amplification'].astype(int)
    pooldf['dap-experiment-date'] = pooldf['dap_date'].astype(str).str.split(" ", expand=True)[0]
    pooldf.drop(columns=["dap_date"], inplace=True)
    pooldf.reset_index(drop=True, inplace=True)
    cols = [i for i in pooldf.columns if "tf" in i]
    tfdf =  pooldf.loc[:,cols]
    tfdf.columns = tfdf.copy().columns.str.replace("tf_", "")
    tfdict = tfdf.to_dict('index')[0]
    clarityid = pooldf['submitted-by-cid'][0]
    pooldict = pooldf[['molarity-pm', 'avg-template-size-bp', 'concentration-ng-ul', 'volume-ul']].to_dict('index')[0]
    expdict = pooldf[["dap-experiment-date", "number-pcr-cycles-dap-amplification"]].to_dict('index')[0]
    return tfdict, pooldict, expdict, clarityid
  
    
def create_plates_json(tfdf, sampleinfo, tfdir, tfinfo):
    masterlist = []
    for i in range(len(tfdf)):
        i7plate = tfdf.i7plate.unique()[i]
        tfplate = tfdf.tfplate.unique()[i]
        tfwells = pd.read_csv(tfdir + "/" + tfplate + ".csv")
        tfwells.rename(columns={"well_position":"well-location", "protein_id":"protein-id", "group_name":"group-name", "dna_sequence":"dna-sequence","synbio_construct_name":"synbio-construct-name"}, inplace=True)
        tfwells = tfwells[["well-location", "protein-id", "source", "nickname", "group-name", "dna-sequence", "synbio-construct-name"]].sort_values(by=['well-location'])
        tfwells = tfwells.fillna("none")
        protein_plate = {}
        protein_plate['plate-barcode'] = tfplate
        reference = tfinfo['reference']
        protein_plate['barcode-dap-lib-ref'] = tfplate + "_" + reference
        wellsinfo = []
        # For each Protein Plate, Loop through All Wells
        for x in range(len(tfwells)):
            well = tfwells.iloc[x]['well-location']
            wellinfo = tfwells.iloc[x].to_dict()
            tfname = tfwells.iloc[x]['protein-id']
            tfspecies = tfinfo['genus'] + tfinfo['species'] + tfinfo['strain']
            org_indexes = []
            # For each well, Loop through all samples
            for j in range(len(sampleinfo)):
                gdnaspecies = sampleinfo.iloc[j]['genus'] + sampleinfo.iloc[j]['species'] + sampleinfo.iloc[j]['strain']
                sampleid = sampleinfo.iloc[j]['sample-id']
                i5 = sampleinfo.iloc[j]['i5']
                # This is the unique name that distinguises all individual combinations of sample / protein ()
                indexname = i7plate + well + "-" + i5
                orgindex = {}
                orgindex['sample-id'] = int(sampleid)
                orgindex['index-name'] = indexname
                dmlibname = tfname + "_" + tfspecies + "_" + gdnaspecies + "_" + str(datetime.datetime.now()).split(".")[0].replace(" ", "_")
                orgindex['demultiplexed-library-name'] = dmlibname
                org_indexes.append(orgindex)
            wellinfo['organism-indexes'] = org_indexes
            wellsinfo.append(wellinfo)
        protein_plate['wells'] = wellsinfo
        masterlist.append(protein_plate)
    return masterlist
# Initialize dictionary and add CID
tfinfo, poolinfo, experimentinfo, submit_cid = get_experiment_info(dapsheet)
jdap = {}
jdap['submitted-by-cid'] = submit_cid

# Load pool and experiment info
jdap['TF-DNA-source'] = tfinfo
jdap['dap-experiment-date'] = experimentinfo['dap-experiment-date']
jdap['number-pcr-cycles-dap-amplification'] = experimentinfo['number-pcr-cycles-dap-amplification']
jdap['dap-pool'] = poolinfo

# Load Sample Info
sampleinfo = get_sample_info(spitpath, dapsheet)
jdap['organisms'] = [sampleinfo.drop(columns="i5").iloc[i].to_dict() for i in range(len(sampleinfo))]

# Load Protein Plate Info
tfdf =  get_tfplates(dapsheet)

# This is the directory that stores all protein plates with naming convention {$plateid}.csv
tfdir = os.path.join(os.getcwd(), "tfplates")

# Create the protein plate wells * samples json
protein_json = create_plates_json(tfdf, sampleinfo, tfdir, tfinfo)
jdap['protein-plates'] = protein_json

# Write to file

with open(outfile, "w") as f:
    json.dump(jdap, f, indent=4, default=convert)
    
print("True")