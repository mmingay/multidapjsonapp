import json
import os
import pandas as pd
import datetime
import sys

spitpath = sys.argv[1]
dapsheet = sys.argv[2]

def get_spitis(filepath):
    spsamples = pd.read_excel(filepath, sheet_name="DATA SHEET")
    # tcols = spsamples.columns[[5,6,7,2,1,0,3]]
    tcols = ['Genus*', 'Species*', 'Strain* (required for microbial products)', 'Sample ID (No edit)', 'Seq Project Name (No edit)', 'Seq Project ID (No edit)', 'Sample Name*']
    # Get rid of first 2 rows
    subsamples = spsamples.loc[2:, tcols]
    # subsamples.columns = ['genus', 'species', 'strain', 'sample-id', 'sequencing-project-name', 'sequencing-project-id', 'sample-name']
    # Create a unique identified for each sample to match with other sheet
    subsamples.rename(columns={"Genus*": "genus", "Species*": "species", "Strain* (required for microbial products)": "strain",
                              "Sample ID (No edit)": "sample-id", "Seq Project Name (No edit)": "sequencing-project-name", 
                              "Seq Project ID (No edit)": "sequencing-project-id", "Sample Name*":"sample-name"}, inplace=True)
    subsamples['mattid'] = subsamples['genus'].astype(str).str.upper() + "_" + subsamples['species'].astype(str).str.upper() + "_" + subsamples['strain'].astype(str).str.upper() 
    return subsamples.reset_index(drop=True)

def get_subxcel(filepath):
    # get sample info
    xsamples = pd.read_excel(filepath, sheet_name="organisms")
    xsamples['mattid'] = xsamples['Genus'].astype(str).str.upper() + "_" + xsamples['Species'].astype(str).str.upper() + "_" + xsamples['Strain'].astype(str).str.upper()
    return xsamples

def validate_files(spitsheet, dapsheet):
    try:
        spitdf=get_spitis(spitsheet)
        dapdf=get_subxcel(dapsheet)
    except:
        return -1
    if len(set(spitdf['sample-name'])) != len(set(dapdf['Sample Name'])):
        return("different number of samples in DapSheet SPITIS form")
    # check for unique ids 
    if len(set(dapdf['Sample Name'])) != len(dapdf['Sample Name']) or len(set(spitdf['sample-name'])) != len(spitdf['sample-name']):
        return("non unique sample ids are present")
    # make sure sample ids match up
    if [i for i in spitdf['sample-name'].values] !=  [j for j in dapdf['Sample Name'].values]:
        return("sample IDs are different between SPITIS and DapSheet")
    else:
        return True

result = validate_files(spitpath, dapsheet)
print(result)